# UBJSON-JS

## What is UBJSON?

UBJSON is a binary alternative to JSON, fully compatible with it. [It is described here.](http://ubjson.org)

## Why this library?

I found only one JavaScript library for parsing and dumping UBJSON ([by Shelacek](https://bitbucket.org/shelacek/ubjson/src/master/)), but it is made for NodeJS, not fully compatible with browsers and does not handle binary values (in addition, that implementation is too complex).

## Use it

Currently, this library only handles parsing. It is very simple:

    <script type="text/javascript" src="ubjson.js"></script>

    var raw = hex2bin("7b24552355035506666f6f626172ff55054c6f72656d455505697073756d55");
    var data = ubjson_parse(raw);
    // -> Object { foobar: 255, Lorem: 69, ipsum: 85 }

The following features aren't implemented yet:
 * UBJSON encoding
 * Parsing type float32
 * Parsing type float64
 * Parsing type high-precision number

## License

This library is licensed under GNU AGPL v3+.
